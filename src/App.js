import React from "react";
import "./App.css";
import { ProfileEditor } from "./features/edit_profile/index";

function App() {
  return (
    <div className="App">
      <ProfileEditor />
    </div>
  );
}

export default App;
