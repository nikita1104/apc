import React from "react";
import styled, { css } from "styled-components";
import HamburgerSrc from "./images/hamburger.svg";
import ArrowDownSrc from "./images/arrow_down.svg";
import SearchSrc from "./images/search.svg";
import LogoDefaultSrc from "./images/logo_default.svg";
// import DoneSrc from "./images/done.svg";
import { Form, Field } from "react-final-form";
import { isMobile } from "react-device-detect";

export const Ui = () => {
  const Wrapper = styled.div`
    border: 1px solid;
    padding: 10px;
  `;
  return (
    <Wrapper>
      <h3>ui</h3>
      <ViewElement
        title="Кнопка вызова меню"
        component={
          <Button
            config={{
              background: null,
              text: null,
              icon: HamburgerSrc,
              clickFn: () => alert("...")
            }}
          />
        }
      />
      <ViewElement
        title="Ссылка картинкой (логотип)"
        component={
          <Link
            config={{
              type: "logoDefault",
              background: LogoDefaultSrc,
              text: null,
              icon: HamburgerSrc,
              href: "#",
              clickFn: () => alert("...")
            }}
          />
        }
      />
      <ViewElement
        title="Обычная кнопка меню"
        component={
          <Link
            config={{
              type: "mainMenuButton",
              background: null,
              text: "О компании",
              href: "#"
            }}
          />
        }
      />
      <ViewElement
        title="Кнопка смены языка"
        component={
          <Button
            config={{
              type: "lang",
              background: null,
              text: "RUS",
              icon: ArrowDownSrc,
              clickFn: () => alert("...")
            }}
          />
        }
      />
      <ViewElement
        title="Кнопка поиска"
        component={
          <Button
            config={{
              background: null,
              text: null,
              icon: SearchSrc,
              clickFn: () => alert("...")
            }}
          />
        }
      />
      <ViewElement
        title="BreadCrumbs"
        component={
          <BreadCrumbs
            config={{
              items: [
                { name: "Главная", href: "/" },
                { name: "Питание", href: null }
              ]
            }}
          />
        }
      />
      <ViewElement
        title="Заголовок страницы"
        component={<TitleH1>Изменить профиль</TitleH1>}
      />

      <ViewElement
        title="Кнопка смены типа лица"
        component={
          <Selector
            config={{
              text: "Физическое лицо",
              active: true,
              clickFn: () => alert("...")
            }}
          />
        }
      />
      <ViewElement
        title="Заголовок формы"
        component={<FormTitle>Изменить профиль</FormTitle>}
      />

      <ViewElement
        title="Поле ввода"
        component={
          <Form
            onSubmit={() => null}
            render={() => (
              <CustomField
                config={{
                  elementName: "input",
                  name: "phone",
                  placeholder: "Введите телефон",
                  label: "Телефон",
                  validate: value => (value ? undefined : "Required")
                }}
              />
            )}
          />
        }
      />

      <ViewElement
        title="Синяя кнопка"
        component={
          <Button
            config={{
              background: null,
              text: "Сохранить данные",
              icon: null,
              type: "submit",
              clickFn: () => alert("...")
            }}
          />
        }
      />
      <ViewElement
        title="Заголовок подвального меню"
        component={<FooterMenuTitle>Кто мы?</FooterMenuTitle>}
      />
      <ViewElement
        title="Кнопка подвального меню"
        component={
          <Button
            config={{
              background: null,
              text: "История АРС",
              icon: null,
              clickFn: () => alert("...")
            }}
          />
        }
      />
    </Wrapper>
  );
};

export const Button = ({ config }) => {
  const types = {
    hamburger: css`
      img {
        width: 20px;
      }
    `,
    lang: css`
      display: flex;
      align-items: center;
      img {
        width: 8px;
        margin-right: 8px;
      }
    `,
    submit: css`
      padding: 17px;
      background: #56ccf2;
      font-weight: 500;
      font-size: 12px;
      line-height: 16px;
      text-align: center;
      letter-spacing: 0.03em;
      text-transform: uppercase;
      color: #ffffff;
    `
  };
  const StyledButton = styled.button`
    display: block;
    img {
      display: block;
    }
    ${p => types[p.type]}
  `;
  return (
    <StyledButton onClick={config.clickFn} type={config.type}>
      {config.icon && <img src={config.icon} alt="" />}
      {config.text}
    </StyledButton>
  );
};

export const Selector = ({ config }) => {
  const types = {
    active: css`
      cursor: default;
      border-color: #56cd51;
    `,
    inactive: css`
      /* background: red; */
      color: #aaadb6;
      border-color: #fff0;
    `
  };
  const Wrapper = styled.button`
    padding-bottom: 26px;
    border-bottom: 2px solid;
    ${p => (p.active ? types.active : types.inactive)}
    margin-right: 35px;
  `;
  const Text = styled.span`
    ${isMobile
      ? css`
          font-size: 16px;
          line-height: 20px;
        `
      : css`
          font-size: 20px;
          line-height: 24px;
        `}
  `;
  return (
    <Wrapper active={config.active} onClick={config.clickFn}>
      <Text>{config.text}</Text>
    </Wrapper>
  );
};

export const Link = ({ config }) => {
  const types = {
    logoDefault: isMobile
      ? css`
          width: 101px;
          height: 29px;
        `
      : css`
          width: 132px;
          height: 38px;
        `,
    mainMenuButton: css`
      font-weight: 500;
      font-size: 12px;
      line-height: 16px;
      letter-spacing: 0.03em;
      text-transform: uppercase;
      color: #63676b;
    `
  };
  const A = styled.a`
    display: block;
    cursor: pointer;
    color: inherit;
    ${p => types[p.type]}
    ${p =>
      p.background &&
      css`
        background-image: url(${p.background});
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
      `}
  `;
  return (
    <A
      type={config.type}
      background={config.background}
      href={config.href}
      target={config.target ? "_blank" : "_self"}
    >
      {config.text}
    </A>
  );
};

export const Container = styled.div`
  max-width: 1386px;
  margin: 0 auto;
  padding: 0 10px;
`;
export const Column = styled.div`
  width: 50%;
  :first-child {
    border-right: 1px solid #e0e2e9;
    padding-right: 100px;
  }
  :last-child {
    padding-left: 100px;
  }
`;

export const BreadCrumbs = ({ config }) => {
  const Wrapper = styled.div`
    display: flex;
    align-items: center;
    margin: 21px 0 30px;
  `;
  const A = styled.a`
    font-family: Roboto;
    font-weight: normal;
    font-size: 10px;
    line-height: 14px;
    color: #aaadb6;
  `;
  const Separator = styled.i`
    :before {
      content: "/";
      font-family: Roboto;
      font-weight: normal;
      font-size: 10px;
      line-height: 14px;
      color: #aaadb6;
      margin: 0 5px;
      display: block;
    }
  `;
  const Item = ({ children }) => children;
  // const Item = ({ children }) => <Span>{children}</Span>;
  return (
    <Wrapper>
      {config.items.map((x, i) => (
        <Item key={i}>
          <A href={x.href}>{x.name}</A>
          {i < config.items.length - 1 && <Separator />}
        </Item>
      ))}
    </Wrapper>
  );
};
// font-family: Oswald;
// font-style: normal;
// font-weight: normal;
// font-size: 20px;
// line-height: 24px;
// /* or 120% */

// color: #000000;

export const TitleH1 = styled.h1`
  ${isMobile
    ? css`
        font-size: 20px;
        line-height: 24px;
        margin-bottom: 20px;
      `
    : css`
        font-size: 28px;
        line-height: 36px;
        margin-bottom: 30px;
      `}
`;
const FormTitle = styled.h3``;
const FooterMenuTitle = styled.h3``;

const CustomFieldWrapper = styled.div`
  display: flex;
  /* align-items: center; */
  justify-content: space-between;
  position: relative;
  label {
    font-size: 14px;
    line-height: 18px;
    letter-spacing: 0.01em;
    margin-top: 10px;
  }
  input,
  textarea {
    border: 1px solid #e0e2e9;
    padding: 6px;
    width: 271px;
  }
  textarea {
    height: 124px;
    resize: none;
  }
`;
const FieldInner = styled.div`
  :after {
    content: "";
    background-image: url(/static/media/done.354a97ad.svg);
    background-size: cover;
    width: 18px;
    height: 18px;
    display: block;
    position: absolute;
    top: 0;
    left: calc(100% + 13px);
    top: 7px;
  }
`;
export const CustomField = ({ config }) => {
  return (
    <CustomFieldWrapper className="field">
      <label>{config.label}</label>
      <Field name={config.name} validate={config.validate}>
        {({ input, meta }) => (
          <FieldInner>
            {config.elementName === "input" && (
              <input type="text" {...input} placeholder={config.placeholder} />
            )}
            {config.elementName === "textarea" && (
              <textarea type="text" {...input} />
            )}
            {meta.touched && meta.error && <span>{meta.error}</span>}
            {/* <Done /> */}
          </FieldInner>
        )}
      </Field>
    </CustomFieldWrapper>
  );
};

/*************************************************************** */
/*************************************************************** */
/*************************************************************** */
/*************************************************************** */
const ViewElement = ({ children, component, title }) => {
  const Wrapper = styled.div`
    border: 1px solid;
    padding: 15px;
    margin: 10px;
  `;
  const Title = styled.h4`
    font-weight: bold;
    color: #3a5d8a;
    padding: 2px;
    margin-bottom: 10px;
  `;
  const Container = styled.div`
    border: 1px dotted;
  `;
  const Props = styled.pre`
    margin-top: 10px;
  `;
  return (
    <Wrapper>
      <Title>{title}</Title>
      <Container>{component}</Container>
      <Props>{JSON.stringify(component.props, 2, 2)}</Props>
    </Wrapper>
  );
};
