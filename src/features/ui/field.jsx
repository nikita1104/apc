import React from "react";
import styled, { css } from "styled-components";
import { Field as FinalFormField } from "react-final-form";
import CheckMarkSrc from "../../images/check_mark.svg";
import DonekSrc from "../../images/done.svg";
import RedCrossSrc from "../../images/red_cross.svg";
import { isMobile } from "react-device-detect";
export const Field = ({ config }) => {
  return (
    <CustomFieldWrapper className="field">
      {!isMobile && <span className="field-name">{config.label}</span>}
      <FinalFormField
        name={config.name}
        validate={config.validate}
        type={config.type}
      >
        {({ input, meta }) => (
          <div className="field-inner" onKeyUp={config.keyUpFn}>
            {config.type === "input" && (
              <input
                className="input-text"
                type="text"
                {...input}
                placeholder={config.placeholder}
              />
            )}
            {config.type === "textarea" && (
              <textarea
                className="textarea"
                type="text"
                {...input}
                placeholder={config.placeholder}
              />
            )}
            {config.type === "checkbox" && (
              <div className="checkbox-area">
                <label>
                  <Checkmark className="checkmark" checked={input.checked} />
                  <input
                    type="checkbox"
                    {...input}
                    component="input"
                    style={{ display: "none" }}
                  />
                </label>
                <p className="description">{config.description}</p>
              </div>
            )}
            {/* {meta.touched && meta.error && (
              <span className="error">{meta.error}</span>
            )} */}
            <div className="sub">{config.sub}</div>
            {config.validate && meta.touched && (
              <ValidMarker isError={meta.error} />
            )}
          </div>
        )}
      </FinalFormField>
    </CustomFieldWrapper>
  );
};

const CustomFieldWrapper = styled.div`
  ${isMobile
    ? css`
        display: block;
      `
    : css`
        display: flex;
        justify-content: space-between;
      `}

  position: relative;
  .field-inner {
    position: relative;

    .error {
      position: absolute;
      bottom: 0;
      right: 0;
      color: #e54238;
      font-size: 10px;
      margin: 10px;
    }
    .sub {
      position: absolute;
    }
  }
  .field-name {
    font-size: 14px;
    line-height: 18px;
    letter-spacing: 0.01em;
    margin-top: 10px;
  }
  .input-text {
    border: 1px solid #e0e2e9;
    padding: 6px;
    width: ${isMobile ? "100%" : "271px"};
  }
  .textarea {
    border: 1px solid #e0e2e9;
    padding: 6px;
    width: ${isMobile ? "100%" : "271px"};
    height: ${isMobile ? "35px" : "124px"};
    resize: none;
  }
  .checkbox-area {
    display: flex;
    width: ${isMobile ? "100%" : "271px"};
    padding: 6px;

    input:checked ~ .checkmark {
      background-color: #2196f3;
    }
    input:checked {
      display: block;
    }
    .description {
      font-family: Roboto;
      font-style: normal;
      font-weight: normal;
      font-size: 10px;
      line-height: 14px;
      color: #63676b;
    }
  }
`;

const Checkmark = styled.div`
  width: 14px;
  height: 14px;
  display: block;
  border: 1px solid #56cd51;
  box-sizing: border-box;
  border-radius: 2px;
  margin-right: 11px;
  cursor: pointer;
  ${p =>
    p.checked &&
    css`
      background-image: url(${CheckMarkSrc});
      background-repeat: no-repeat;
      background-position: center;
      background-size: 8px;
    `}
`;
const ValidMarker = styled.div`
  background-repeat: no-repeat;
  width: 18px;
  height: 18px;
  display: block;
  position: absolute;
  top: 0;
  top: 7px;
  border: 1px solid;
  border-radius: 50%;
  background-repeat: none;
  background-position: center;
  background-size: 7px;
  ${isMobile
    ? css`
        right: 13px;
      `
    : css`
        left: 100%;
        margin-left: 13px;
      `}
  ${p =>
    p.isError
      ? css`
          /* margin-right: 100px; */
          /* background: red; */
          background-image: url(${RedCrossSrc});
          border-color: #e54238;
        `
      : css`
          border-color: #56cd51;
          /* background: green; */

          background-image: url(${DonekSrc});
        `};
`;
