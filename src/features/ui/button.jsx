import React from "react";
import styled, { css } from "styled-components";
import { isMobile } from "react-device-detect";
export const Button = ({ config }) => {
  return (
    <StyledButton onClick={config.clickFn} type={config.type}>
      {config.icon && <img src={config.icon} alt="" />}
      {config.text}
    </StyledButton>
  );
};

const types = {
  hamburger: css`
    img {
      width: 20px;
    }
    margin-right: ${isMobile ? "10px" : "28px"};
  `,
  withLeftIcon: isMobile
    ? css`
        display: flex;
        align-items: center;
        font-weight: 500;
        font-size: 10px;
        line-height: 12px;
        letter-spacing: 0.03em;
        text-transform: uppercase;
        img {
          width: 11px;
          margin-right: 16px;
        }
      `
    : css`
        display: flex;
        align-items: center;
        font-weight: 500;
        font-size: 10px;
        line-height: 12px;
        letter-spacing: 0.03em;
        text-transform: uppercase;
        color: #63676b;
        img {
          width: 8px;
          margin-right: 8px;
        }
      `,
  submit: css`
    padding: 17px;
    background: #56ccf2;
    font-weight: 500;
    font-size: 12px;
    line-height: 16px;
    text-align: center;
    letter-spacing: 0.03em;
    text-transform: uppercase;
    color: #ffffff;
  `
};
const StyledButton = styled.button`
  display: block;
  img {
    display: block;
  }
  ${p => types[p.type]}
`;
