import React, { useState } from "react";
import { OnChange } from "react-final-form-listeners";
import { validation, Field } from "../../ui";
import { isMobile } from "react-device-detect";

export const PersonInput = ({ values, setAddressValue }) => {
  const [adressOptions, setAdressOptions] = useState({
    touched: false,
    hints: []
  });

  const fieldHandle = async value => {
    const query = value;
    const response = await fetch(
      "http://109.68.190.47:9090/item/address/suggestions",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: "831d3a50-ae7d-4362-abb0-81f0c6e7dee0"
        },
        body: JSON.stringify({ query })
      }
    ).then(x => x.json());
    setAdressOptions(
      Array.isArray(response)
        ? { touched: false, hints: response }
        : { touched: false, hints: [] }
    );
  };
  const clickAdressHandle = adress => {
    setAddressValue(adress);
    setAdressOptions({ touched: true, hints: [] });
  };
  return (
    <div className="inner">
      <div className="column">
        <h3 className="column-name">Личные данные</h3>
        <OnChange name="address">
          {(value, previous) => {
            if (adressOptions.touched) {
              fieldHandle(value);
              // getAdressHints(value);
            }
          }}
        </OnChange>
        {[
          {
            type: "input",
            name: "first_name",
            label: "Фамилия",
            validate: validation
          },
          {
            type: "input",
            name: "name",
            label: "Имя",
            validate: validation
          },
          {
            type: "input",
            name: "surname",
            label: "Отчество"
          },
          {
            type: "input",
            name: "phone",
            label: "Контактный телефон",
            validate: validation
          },
          {
            type: "checkbox",
            name: "sms",
            label: null,
            description: "Получать смс-сообщения об изменении статусов заказов"
          },
          {
            type: "input",
            name: "additional_phone",
            label: "Дополнительный телефон"
          },
          {
            type: "input",
            name: "email",
            label: "Email"
          }
        ].map((x, i) => (
          <Field
            config={(() => {
              return {
                ...x,
                label: isMobile ? null : x.label,
                placeholder: isMobile ? x.label : x.placeholder
              };
            })()}
            key={i}
          />
        ))}
        <Field
          config={{
            type: "checkbox",
            name: "mailing",
            label: null,
            description:
              "Подписаться на Email-уведомления об Акциях, Скидках, Новостях, новых вопросах нашей Игры."
          }}
        />
      </div>
      <div className="column">
        <h3 className="column-name">Параметры доставки</h3>
        {[
          {
            type: "input",
            name: "region",
            label: "Регион доставки:"
          },
          {
            type: "input",
            name: "metro",
            label: "Метро:"
          },
          {
            type: "input",
            name: "delivery",
            label: "Удобный способ доставки:"
          },
          {
            type: "input",
            name: "pay",
            label: "Удобный способ оплаты:"
          },
          {
            type: "textarea",
            name: "address",
            label: "Адрес доставки *",
            validate: validation,
            keyUpFn: () =>
              setAdressOptions({ ...adressOptions, touched: true }),
            sub: (
              <>
                {adressOptions.hints.length ? (
                  <div className="hints">
                    {adressOptions.hints.map((adress, i) => (
                      <div
                        key={i}
                        onClick={() => clickAdressHandle(adress)}
                        className="hint-adress"
                      >
                        > {adress}
                      </div>
                    ))}
                  </div>
                ) : null}
              </>
            )
          }
        ].map((x, i) => (
          <Field
            config={(() => {
              return {
                ...x,
                label: isMobile ? null : x.label,
                placeholder: isMobile ? x.label : x.placeholder
              };
            })()}
            key={i}
          />
        ))}
      </div>
    </div>
  );
};
