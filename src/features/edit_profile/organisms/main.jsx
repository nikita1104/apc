import React, { useState, useEffect } from "react";
import styled, { css } from "styled-components";
import { Form } from "react-final-form";
import { Selector } from "../../../ui";
import { Button } from "../../ui";
import arrayMutators from "final-form-arrays";
import { $profile } from "../models/getProfile";
import { editProfile } from "../models/editProfile";
import { useStore } from "effector-react";
import { isMobile } from "react-device-detect";
import { PersonInput } from "../molecules/person_input";

export const Main = () => {
  const profile = useStore($profile);
  const [isJusPerson, setIsJurPerson] = useState();
  useEffect(() => {
    if (profile) setIsJurPerson(profile.is_jurperson);
  }, [profile]);
  return (
    <Wrapper>
      <Selector
        config={{
          text: "Физическое лицо",
          active: !isJusPerson,
          clickFn: () => setIsJurPerson(false)
        }}
      />
      <Selector
        config={{
          text: "Юридическое лицо",
          active: isJusPerson,
          clickFn: () => setIsJurPerson(true)
        }}
      />
      <Form
        onSubmit={editProfile}
        mutators={{
          ...arrayMutators,
          setAddressValue: (args, state, utils) => {
            const [val] = args;
            utils.changeValue(state, "address", () => val);
          }
        }}
        initialValues={profile}
        render={({ handleSubmit, form, values }) => (
          <form>
            <div className="fields-area">
              {isJusPerson ? (
                "No company layout..."
              ) : (
                <>
                  <PersonInput
                    values={values}
                    setAddressValue={form.mutators.setAddressValue}
                  />
                  <div className="button-area">
                    <Button
                      config={{
                        text: "Сохранить данные",
                        icon: null,
                        type: "submit",
                        clickFn: handleSubmit
                      }}
                    />
                  </div>
                </>
              )}
            </div>
          </form>
        )}
      />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  border-bottom: 1;
  .fields-area {
    ${isMobile
      ? null
      : css`
          padding: 80px 80px 47px;
          border: 1px solid #e0e2e9;
        `};
    .inner {
      display: ${isMobile ? "block" : "flex"};
      .column {
        ${isMobile
          ? css`
              width: 100%;
              padding: 25px 0;
            `
          : css`
              width: 50%;
            `};
        width: ${isMobile ? "100%" : "50%"};
        :first-child {
          ${isMobile
            ? css`
                border-bottom: 1px solid #e0e2e9;
              `
            : css`
                border-right: 1px solid #e0e2e9;
                padding-right: 100px;
              `}
        }
        :last-child {
          ${isMobile
            ? null
            : css`
                padding-left: 100px;
              `}
        }
      }
      .hints {
        background: #fff;
        border: 1px SOLID;
        padding: 10px;
      }
    }
    .field {
      margin-bottom: 16px;
      :last-child {
        margin-bottom: 0;
      }
    }
    .button-area {
      ${isMobile
        ? css`
            margin-top: 5px;
          `
        : css`
            display: flex;
            justify-content: center;
            margin-top: 40px;
          `}

      button {
        ${isMobile &&
          css`
            display: block;
            width: 100%;
            max-width: 500px;
          `}
      }
    }
    .column-name {
      font-weight: normal;
      ${isMobile
        ? css`
            font-size: 16px;
            line-height: 20px;
            color: #63676b;
          `
        : css`
            font-size: 20px;
            line-height: 24px;
          `}
      margin-bottom: 10px;
    }
    .hint-adress {
      cursor: pointer;
    }
  }
`;
