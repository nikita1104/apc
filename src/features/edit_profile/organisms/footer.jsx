import React from "react";
import styled, { css } from "styled-components";
import { Container } from "../../../ui";
import LogoSrc from "../../../images/logo_footer.svg";
import PaymentSrc from "../../../images/payment.svg";
import FacebookSrc from "../../../images/facebook.svg";
import TwitterSrc from "../../../images/twitter.svg";
import YoutubeSrc from "../../../images/youtube.svg";
import InstagramSrc from "../../../images/instagram.svg";
import { isMobile } from "react-device-detect";
export const Footer = () => (
  <Wrapper>
    <div>
      <Container>
        <div className="main">
          <ContentColumn title="Кто мы?" order="3">
            {[
              {
                name: "История APC",
                href: "#"
              },
              {
                name: "Для инвесторов",
                href: "#"
              },
              {
                name: "Карьера",
                href: "#"
              },
              {
                name: "Устойчивое развитие",
                href: "#"
              }
            ].map((x, i) => (
              <MenuLink key={i} data={x} />
            ))}
          </ContentColumn>
          <ContentColumn title="Каталог продукции" order="1">
            {[
              {
                name: "Питание",
                href: "#"
              },
              {
                name: "Охлаждение",
                href: "#"
              },
              {
                name: "Безопасность",
                href: "#"
              },
              {
                name: "Стоечные Системы",
                href: "#"
              }
            ].map((x, i) => (
              <MenuLink key={i} data={x} />
            ))}
          </ContentColumn>
          <ContentColumn title={null} order="2">
            {[
              {
                name: "Модули данных",
                href: "#"
              },
              {
                name: "Data Center",
                href: "#"
              },
              {
                name: "Сервисы",
                href: "#"
              },
              {
                name: "Распределение питания",
                href: "#"
              }
            ].map((x, i) => (
              <MenuLink key={i} data={x} />
            ))}
          </ContentColumn>
          <ContentColumn title="покупателю" order="4">
            {[
              {
                name: "Пресса",
                href: "#"
              },
              {
                name: "APC Блог",
                href: "#"
              },
              {
                name: "Блог Schneider Electric",
                href: "#"
              },
              {
                name: "Свяжитесь с нами",
                href: "#"
              }
            ].map((x, i) => (
              <MenuLink key={i} data={x} />
            ))}
          </ContentColumn>
          <ContentColumn
            title="Каталог продукции"
            className="contacts-column"
            order="5"
          >
            <Contacts />
          </ContentColumn>
        </div>
      </Container>
    </div>
    <div className="separator" />
    <div>
      <Container>
        <div className="sub">
          <div className="items">
            <img src={LogoSrc} alt="" className="logo" />
            <span className="copyright">2019, APC by Schneider Electric</span>
          </div>
          <div className="items">
            <div className="menu">
              {[
                {
                  href: "#",
                  text: "Положения и условия"
                },
                {
                  href: "#",
                  text: "Политика обработки персональных данных"
                },
                {
                  href: "#",
                  text: "Политика cookie-файлов"
                }
              ].map((x, i) => (
                <a className="terms-link" href={x.href} key={i}>
                  {x.text}
                </a>
              ))}
            </div>
            <img src={PaymentSrc} alt="" className="payment" />
          </div>
        </div>
      </Container>
    </div>
  </Wrapper>
);

const ContentColumn = ({ title, children, className, order }) => (
  <Column className={`column ${className && className}`} order={order}>
    <h3 className="title">{title}</h3>
    <div>{children}</div>
  </Column>
);

const MenuLink = ({ data }) => (
  <a className="menu-link" href={data.href}>
    {data.name}
  </a>
);

const Contacts = () => (
  <div className="contacts-area">
    <a href="tel:+7 (495) 671-71-50" className="phone">
      +7 (495) 671-71-50
    </a>
    <p className="adress">
      119415г. Москва, Проспект <br />
      Вернадского, д. 39
    </p>
    <div className="smm">
      {[
        {
          href: "#",
          src: FacebookSrc
        },
        {
          href: "#",
          src: TwitterSrc
        },
        {
          href: "#",
          src: YoutubeSrc
        },
        {
          href: "#",
          src: InstagramSrc
        }
      ].map((x, i) => (
        <a href={x.href} className="smm-link" key={i}>
          <img src={x.src} alt="" />
        </a>
      ))}
    </div>
  </div>
);

const Column = styled.div`
  ${p =>
    p.order &&
    css`
      order: ${p.order};
    `}
`;

const Wrapper = styled.div`
  padding: 65px 0 0;
  .separator {
    ${isMobile
      ? css``
      : css`
          border-bottom: 1px solid #e0e2e9;
          margin: 60px 0 10px;
        `}
  }
  .main {
    display: flex;
    ${isMobile &&
      css`
        flex-wrap: wrap;
        justify-content: center;
      `}
    ${Column} {
      ${isMobile
        ? css`
            width: 50%;
            margin-bottom: 30px;
          `
        : css`
            width: 20%;
          `}

      .title {
        font-weight: 500;
        font-size: 12px;
        line-height: 16px;
        letter-spacing: 0.03em;
        text-transform: uppercase;
        margin-bottom: 15px;
        height: 16px;
      }
      .menu-link {
        font-family: Roboto;
        font-style: normal;
        font-weight: normal;
        ${isMobile
          ? css`
              font-size: 11px;
              line-height: 14px;
            `
          : css`
              font-size: 14px;
              line-height: 18px;
            `}
        letter-spacing: 0.01em;
        color: #63676b;
        display: block;
        margin-bottom: 10px;
        :last-child {
          margin-bottom: 0;
        }
      }
      .contacts-area {
        .phone {
          font-weight: 500;
          font-size: 12px;
          line-height: 16px;
          letter-spacing: 0.03em;
          text-transform: uppercase;
          color: #000000;
          margin-bottom: 10px;
          display: block;
        }
        .adress {
          font-family: Roboto;
          font-style: normal;
          font-weight: normal;
          font-size: 11px;
          line-height: 14px;
          color: #63676b;
          margin-bottom: 20px;
        }
        .smm {
          .smm-link {
            display: inline-flex;
            margin-right: 16px;
          }
        }
      }
    }
    .column.contacts-column {
      ${isMobile
        ? css`
            text-align: center;
            width: 100%;
          `
        : null};
    }
  }
  .sub {
    display: flex;
    justify-content: space-between;
    align-items: center;
    .copyright {
      font-family: Roboto;
      font-style: normal;
      font-weight: normal;
      font-size: 11px;
      line-height: 14px;
      color: #aaadb6;
      ${isMobile
        ? css`
            margin-top: 15px;
          `
        : css``}
    }
    ${isMobile
      ? css`
          flex-direction: column-reverse;
        `
      : css``}
    padding-bottom: 20px;
    .items {
      ${isMobile
        ? css`
            flex-direction: column;
          `
        : css``}
      display: flex;
      align-items: center;
    }
    .logo {
      margin-right: 33px;
    }
    .payment {
      ${isMobile ? css`` : css``}
    }
    .menu {
      display: flex;
      ${isMobile
        ? css`
            flex-direction: column;
            align-items: center;
            margin-bottom: 22px;
          `
        : css`
            margin-right: 70px;
          `}
      .terms-link {
        display: block;
        font-family: Roboto;
        font-style: normal;
        font-weight: normal;
        font-size: 11px;
        line-height: 14px;
        color: #63676b;
        margin-right: 15px;
      }
    }
  }
`;
