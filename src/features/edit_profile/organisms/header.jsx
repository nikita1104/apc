import React from "react";
import styled from "styled-components";
import { Link, Container } from "../../../ui";
import { Button } from "../../ui";
import HamburgerSrc from "../../../images/hamburger.svg";
import LogoDefaultSrc from "../../../images/logo_default.svg";
import SearchSrc from "../../../images/search.svg";
import ArrowDownSrc from "../../../images/arrow_down.svg";
import CartSrc from "../../../images/cart.svg";
import PersonSrc from "../../../images/person.svg";
import { isMobile } from "react-device-detect";

export const Header = () => (
  <Wrapper>
    <div className="row">
      <Container>
        <div className="flex-box">
          <div className="button-group">
            <Button
              config={{
                type: "hamburger",
                icon: HamburgerSrc,
                clickFn: () => alert("open menu")
              }}
            />
            <Link
              config={{
                type: "logoDefault",
                background: LogoDefaultSrc,
                icon: HamburgerSrc,
                href: "#",
                clickFn: () => alert("...")
              }}
            />
          </div>
          {!isMobile && (
            <div className="main-menu">
              {[
                {
                  text: "О компании",
                  href: "#"
                },
                {
                  text: "Услуги",
                  href: "#"
                },
                {
                  text: "Доставка",
                  href: "#"
                },
                {
                  text: "обзоры",
                  href: "#"
                },
                {
                  text: "Оплата",
                  href: "#"
                },
                {
                  text: "КОНТАКТЫ",
                  href: "#"
                }
              ].map((x, i) => (
                <Link
                  config={{
                    type: "mainMenuButton",
                    text: x.text,
                    href: x.href
                  }}
                  key={i}
                />
              ))}
            </div>
          )}
          <div className="button-group bar-items">
            {!isMobile && (
              <Button
                config={{
                  type: "withLeftIcon",
                  text: "RUS",
                  icon: ArrowDownSrc,
                  clickFn: () => alert("...")
                }}
              />
            )}

            {[
              !isMobile && {
                icon: SearchSrc,
                clickFn: () => alert("...")
              },
              {
                icon: CartSrc,
                clickFn: () => alert("...")
              },
              {
                icon: PersonSrc,
                clickFn: () => alert("...")
              }
            ].map((x, i) => (
              <Button
                key={i}
                config={{
                  icon: x.icon,
                  clickFn: x.clickFn
                }}
              />
            ))}
          </div>
        </div>
      </Container>
    </div>
    {isMobile && (
      <div className="row">
        <Container>
          <div className="flex-box">
            <Button
              config={{
                type: "withLeftIcon",
                text: "каталог товаров",
                icon: ArrowDownSrc,
                clickFn: () => alert("...")
              }}
            />
            <Button
              config={{
                icon: SearchSrc,
                clickFn: () => alert("...")
              }}
            />
          </div>
        </Container>
      </div>
    )}
  </Wrapper>
);

const Wrapper = styled.div`
  .row {
    border-bottom: 1px solid #e0e2e9;
    padding: 15px 0 10px;
  }
  .flex-box {
    display: flex;
    justify-content: space-between;
    align-items: center;
    .button-group {
      display: flex;
      align-items: center;
    }
    .button-group.bar-items {
      button {
        margin-left: 36px;
        :first-child {
          margin-left: 0;
          margin-right: 20px;
        }
      }
    }
    .main-menu {
      display: flex;
      align-items: center;
      a {
        margin-right: 36px;
        :last-child {
          margin-right: 0;
        }
      }
    }
  }
`;
