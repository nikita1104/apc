import React from "react";
import { BreadCrumbs, TitleH1, Container } from "../../ui";
import { Main } from "./organisms/main";
import { Footer } from "./organisms/footer";
import { Header } from "./organisms/header";

export const ProfileEditor = () => {
  return (
    <div>
      <Header />
      <Container>
        <BreadCrumbs
          config={{
            items: [
              { name: "Главная", href: "/" },
              { name: "Питание", href: null }
            ]
          }}
        />
        <TitleH1>Изменить профиль</TitleH1>
        <Main />
      </Container>
      <Footer />
    </div>
  );
};
