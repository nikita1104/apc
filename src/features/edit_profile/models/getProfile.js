import { createStore, createEvent, createEffect } from "effector";

const setProfileData = createEvent();
const $profile = createStore(null).on(setProfileData, (_, state) => state);

const getProfileData = createEffect().use(async () => {
  const response = await fetch("http://109.68.190.47:9090/user/profile", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "831d3a50-ae7d-4362-abb0-81f0c6e7dee0"
    }
  }).then(x => x.json());
  setProfileData(response);
});
$profile.watch(x => {
  if (x === null) getProfileData();
});

export { getProfileData, $profile };
