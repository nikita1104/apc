import { createEffect } from "effector";

const editProfile = createEffect().use(async values => {
  await fetch("http://109.68.190.47:9090/user/profile/edit", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "831d3a50-ae7d-4362-abb0-81f0c6e7dee0"
    },
    body: JSON.stringify(values)
  }).then(x => x.json());
});

export { editProfile };
